/* Provides cleaner interface for system calls,
 * rather than using the ioctl function directly.*/
#include "temp_sys_calls.h"
#include <fcntl.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <stdio.h>
void mynstimeofday(struct timespec *date)
{
	ioctl(MOD_FD, 0x12345678, date);
	return;
}
void log_dmesg(const char *tag, const pid_t procPID,
		const struct timespec start, const struct timespec finish)
{
	char buf[150];
	// Print information to a buffer and dump the results into the system call.
	sprintf(buf, "%s %d %d.%09d, %d.%09d\n",
	        tag, (int)procPID, (int)start.tv_sec, (int)start.tv_nsec,
	        (int)finish.tv_sec, (int)finish.tv_nsec);
	ioctl(MOD_FD, 0x22345678, buf);
	return;
}

