/* Kernel module for system calls used in the scheduler. */
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/miscdevice.h>
#include <linux/timekeeping.h>
#include <asm/uaccess.h>

#define __NSTIME_IOCTL 0x12345678
#define __DMESG_LOG_IOCTL 0x22345678

static int sch_open(struct inode *inode, struct file *file){return 0;}
static int sch_close(struct inode *inode, struct file *file){return 0;}
static long sch_ioctl(struct file *file, unsigned int ioctl_num, unsigned long ioctl_param);

static struct file_operations sch_fops = {
	.owner = THIS_MODULE,
	.unlocked_ioctl = sch_ioctl,
	.open = sch_open,
	.release = sch_close
};
static struct miscdevice sch_dev = {
	.minor = MISC_DYNAMIC_MINOR,
	.name = "schmods",
	.fops = &sch_fops
};

static int __init sch_init(void)
{
	int ret;
	if((ret = misc_register(&sch_dev)) < 0){
		return ret;
	}
	printk(KERN_INFO "schmods initialized.\n");
	return 0;
}
static void __exit sch_exit(void)
{
	misc_deregister(&sch_dev);
	printk(KERN_INFO "schmods exited.\n");
}
module_init(sch_init);
module_exit(sch_exit);

static long sch_ioctl(struct file *file, unsigned int ioctl_num, unsigned long ioctl_param)
{
	long ret = -EINVAL;
	char buf[150];
	struct timespec now;
	switch(ioctl_num){
		case(__NSTIME_IOCTL): //ioctl_num for mynstimeofday()
			getnstimeofday(&now);
			if(copy_to_user((struct timespec*)ioctl_param, &now,
						sizeof(struct timespec)) < 0){
				return -ENOMEM;
			}
			break;
		case(__DMESG_LOG_IOCTL): //ioctl_num for log_dmesg()
			if(copy_from_user(buf, (char*)ioctl_param, 150) < 0){
				return -ENOMEM;
			}
			printk(KERN_WARNING "%s", buf);
			break;
	}
	ret = 0;
	return ret;
}

