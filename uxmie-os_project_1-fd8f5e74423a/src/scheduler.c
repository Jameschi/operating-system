#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>
#include <sched.h>
#include <fcntl.h>
#include "pcproject.h"
#include "temp_sys_calls.h"

// Get the time it take to run a loop.
void get_quantum(struct timespec *interval){
	int measure = 200;
	long long int int_nsec;
	struct timespec start, finish;
	struct sched_param schFIFO;
	schFIFO.sched_priority = sched_get_priority_min(SCHED_FIFO);
	
	//Set to SCHED_FIFO.
	if(sched_setscheduler(getpid(), SCHED_FIFO, &schFIFO) != 0){
		fprintf(stderr, "setscheduler failed.1\n");
	}
	//Run [measure] loops.
	mynstimeofday(&start);
	run_quantum(measure);
	mynstimeofday(&finish);
	//Set priority back to normal.
	schFIFO.sched_priority = sched_get_priority_max(SCHED_OTHER);
	if(sched_setscheduler(getpid(), SCHED_OTHER, &schFIFO) != 0){
		fprintf(stderr, "setscheduler failed.2\n");
	}
	
	//Divide the time taken by [measure]. Store the result and return.
	int_nsec  = (long long int)(finish.tv_sec - start.tv_sec)*1000000000;
	int_nsec += (long long int)(finish.tv_nsec - start.tv_nsec);
	int_nsec /= measure;
	interval -> tv_sec  = int_nsec / 1000000000;
	interval -> tv_nsec = int_nsec % 1000000000;
	return;
}

int main()
{
	char            S[10], buf[100];
	int             N;
	int             i;
	process_info    *process_list;
	process_info    **proc_ptr;
	struct timespec	qt_interval;

	//Load kernel module.
	if((MOD_FD = open("/dev/schmods", O_RDONLY)) < 0){
		fprintf(stderr, "MODULE NOT FOUND.\n");
		return 1;
	}
	//Find loop time.
	get_quantum(&qt_interval);
	//printf("%d %d\n", (int)qt_interval.tv_sec, (int)qt_interval.tv_nsec);//debug

	//Parse input.
	fgets(S, sizeof(S), stdin);
	scanf("%d\n", &N);
	process_list    = (process_info*)malloc(N*sizeof(process_info));
	proc_ptr        = (process_info**)malloc(N*sizeof(process_info*));
	for(i = 0; i < N; i++){
		fgets(buf, sizeof(buf) - 1, stdin);
		sscanf(buf, "%s %d %d",	process_list[i].name,
		                        &process_list[i].start_time,
		                        &process_list[i].running_time);
		proc_ptr[i] = &process_list[i];
	}
	for(i = 0; S[i] >= 'A' && S[i] <= 'Z'; i++);
	S[i] = 0;

	//Select scheduler.
	if(!strcmp(S, "FIFO")){
		sch_fifo(proc_ptr, N, qt_interval);
	} else if(!strcmp(S, "RR")){
		sch_rr(proc_ptr, N, qt_interval);
	} else if(!strcmp(S, "SJF")){
		sch_sjf(proc_ptr, N, qt_interval);
	} else if(!strcmp(S, "PSJF")){
		sch_psjf(proc_ptr, N, qt_interval);
	} else {
		fprintf(stderr, "The scheduling policy \"%s\" does not exist.\n", S);
	}

	//Ending cleanup.
	free(proc_ptr);
	free(process_list);

	close(MOD_FD);
	return 0;
}
