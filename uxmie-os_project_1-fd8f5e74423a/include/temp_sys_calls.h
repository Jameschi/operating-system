/* Declaration of system calls and data associated with it. */
#pragma once
#include <time.h>
#include <sys/time.h>
#include <stdio.h>

void mynstimeofday(struct timespec *date); //Get time of day in nanoseconds.
void log_dmesg(const char *tag, const pid_t procPID,
               const struct timespec start,
               const struct timespec finish); //Log in dmesg.
int MOD_FD; //The file descriptor of the module after loading.

