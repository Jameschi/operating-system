/* Data types and functions used in multiple files. */
#pragma once

#include <unistd.h>
#include <time.h>
#include <sys/time.h>

// Type for a process list element.
typedef struct{
	pid_t	pid;
	char	name[40];
	int		start_time;
	int		running_time;
	int		priority;
} process_info;

/* Common functions. */
void run_quantum(int x); //Run x loops of a million iterations.
void get_quantum(struct timespec *interval); //Get time it takes to run a loop.

int cmp_time(const void *a, const void *b); //Compare start time. Used for sorting.
int cmp_runningTime(const void *a, const void *b); //Compare exec time Used for sorting.

/* Scheduler functions */
void sch_fifo(process_info **proc_ptr, const int N, const struct timespec quantum);
void sch_rr(process_info **proc_ptr, const int N, const struct timespec quantum);
void sch_sjf(process_info **proc_ptr, const int N, const struct timespec quantum);
void sch_psjf(process_info **proc_ptr, const int N, const struct timespec quantum);

